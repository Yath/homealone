package com.example.demo.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import java.sql.Timestamp;
import java.util.List;

public class Category {


    private Integer id;

    @Length(max = 30)
    private String name;
    private boolean status;
    private Timestamp createdAt;
    private List<Book> books;

    public Category(){

    }

    public Category(Integer id, String name, boolean status, Timestamp createdAt, List<Book> books) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.createdAt = createdAt;
        this.books = books;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", createdAt=" + createdAt +
                ", books=" + books +
                '}';
    }
}
