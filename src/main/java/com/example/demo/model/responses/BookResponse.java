package com.example.demo.model.responses;

public class BookResponse {

    private String id;
    private String title;
    private String ISBN;
    private String bookImage;
    private String categoryName;


    public BookResponse() {

    }

    public BookResponse(String id, String title, String ISBN, String bookImage, String categoryName) {
        this.id = id;
        this.title = title;
        this.ISBN = ISBN;
        this.bookImage = bookImage;
        this.categoryName = categoryName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "BookResponse{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", ISBN='" + ISBN + '\'' +
                ", bookImage='" + bookImage + '\'' +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
