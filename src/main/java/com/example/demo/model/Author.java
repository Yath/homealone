package com.example.demo.model;

import java.sql.Timestamp;
import java.util.List;

public class Author {

    private Integer id;
    private String name;
    private String contact;
    private boolean status;
    private Timestamp createdAt;
    private List<Book> books;

    public Author() {

    }


    public Author(Integer id, String name, String contact, boolean status, Timestamp createdAt, List<Book> books) {
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.status = status;
        this.createdAt = createdAt;
        this.books = books;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", contact='" + contact + '\'' +
                ", status=" + status +
                ", createdAt=" + createdAt +
                ", books=" + books +
                '}';
    }
}