package com.example.demo.configuration;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
@MapperScan({"com.example.demo.repositories"})
public class DataSourceConfiguration {

    @Bean
//    @Profile("psql")
    public DataSource dataSource(){
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
        driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5432/short_course_db");
        driverManagerDataSource.setUsername("postgres");
        driverManagerDataSource.setPassword("yathhacker");

        return driverManagerDataSource;
    }
//
//    @Bean("datasource")
////    @Profile("inMemoryDB")
//    public DataSource inMemoryDB(){
//        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
//        builder.setType(EmbeddedDatabaseType.H2);
//        builder.addScript("db/schema.sql")
//                .addScript("db/data.sql");
//
//        return builder.build();
//    }


    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager(){
        return new DataSourceTransactionManager(dataSource());
    }


}
