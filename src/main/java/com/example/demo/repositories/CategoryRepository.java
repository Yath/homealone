package com.example.demo.repositories;

import com.example.demo.model.Book;
import com.example.demo.model.Category;
import com.example.demo.repositories.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {


    @SelectProvider(type = CategoryProvider.class, method = "getAllCategoryProvider")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "created_at", property = "createdAt"),
            // this is select many in myBatis
            @Result(column = "id", property = "books", many = @Many(select = "getAllBookByCategory"))
    })
    List<Category> getAllCategory(String name);


    @Select("select * from tb_book where category_id =#{id}")
    @Results({
            @Result(column = "book_image", property = "bookImage"),
            @Result(column = "publish_date", property = "publishDate"),
            @Result(column = "created_at", property = "cratedAt"),
    })
    List<Book> getAllBookByCategory(Integer id);



    @InsertProvider(type = CategoryProvider.class,method = "saveProvider")
    @SelectKey(
            statement = "select currval('tb_category_id_seq') as cur_val",
            before = false,
            keyColumn = "cur_val",
            keyProperty = "category.id",
            resultType = Integer.class,
            statementType = StatementType.PREPARED
    )
    boolean save(@Param("category") Category category);


    @Insert({"<script>" ,
            "insert into tb_category(name) " ,
            "values" ,
                "<foreach collection='categories' item='category' index='cate_index' separator=','> " ,
                    "(" ,
                        "#{category.name} " ,
                    ")",
                "</foreach>",
            "</script>"})
    boolean saveBatch(@Param("categories") List<Category> categories);


}
