package com.example.demo.repositories;


import com.example.demo.model.User;
import com.example.demo.repositories.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {


    //TODO  ======= Static SQL ================

    // Can do like this..
    //   // @Select("select *, created_at as createdAt from tb_user")
    //
    //    @Select("select * from tb_user where status is true")
    //
    //    // Mapping field from database filed name properties class not same name;
    //   @Results({
    //           @Result(column = "created_at",property = "createdAt")
    //   })
    //     List<User> getAllUsers();
    //
    //
    //    @Insert("\n" +
    //            "insert into tb_user (username, fullname, gender, email)\n" +
    //            "values (#{userName},#{fullName},#{gender},#{email});")
    //     int saveUser(User user);
    //
    //
    //    // if have alot field in update user must user @Parem()...
    //    @Update("update tb_user set username=#{userName},fullname=#{fullName},gender=#{gender}" +
    //            ",email=#{email} where id=#{id}")
    //     int updateUser(User user);
    //
    //
    //    @Delete("UPDATE tb_user set status = false where id=#{id}")
    //     int deleteUser(@Param("id") Integer id);

        // Can do like this..
   // @Select("select *, created_at as createdAt from tb_user")

    //TODO================================================================

    //@Select("select * from tb_user where status is true")

    // Mapping field from database filed name properties class not same name;
   @Results({
           @Result(column = "created_at",property = "createdAt")
   })

   // this is Provider
   @SelectProvider(type = UserProvider.class,method = "getAllUserProvider")
     List<User> getAllUsers();


    @Insert("\n" +
            "insert into tb_user (username, fullname, gender, email)\n" +
            "values (#{userName},#{fullName},#{gender},#{email});")
     int saveUser(User user);


    // if have alot field in update user must user @Parem()...
    @Update("update tb_user set username=#{userName},fullname=#{fullName},gender=#{gender}" +
            ",email=#{email} where id=#{id}")
     int updateUser(User user);


    @Delete("UPDATE tb_user set status = false where id=#{id}")
     int deleteUser(@Param("id") Integer id);



}
