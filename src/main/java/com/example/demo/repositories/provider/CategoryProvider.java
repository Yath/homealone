package com.example.demo.repositories.provider;

import com.example.demo.model.Category;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String getAllCategoryProvider(String name){


        return new SQL(){{

            SELECT("*");
            FROM("tb_category c");
            if(name != null){
                System.out.println("ok find...");
                WHERE("c.name ilike '%' || #{name} || '%' ");
            }
            WHERE("c.status is true");

        }}.toString();
    }

    public String saveProvider(@Param("category") Category category){

        return new SQL(){{

            INSERT_INTO("tb_category");
            VALUES("name","#{category.name}");

        }}.toString();

    }



}
