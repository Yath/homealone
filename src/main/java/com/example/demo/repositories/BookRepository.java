package com.example.demo.repositories;

import com.example.demo.model.Book;
import com.example.demo.model.Category;
import com.example.demo.model.responses.BookResponse;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {


    @Select("\n" +
            "SELECT b.id as b_id ,b.title,b.publish_date,b.book_image,b.isbn,b.status b_status,b.created_at, c.id as c_id, c.name,\n" +
            "       c.created_at from tb_book b inner join tb_category c on b.category_id=c.id")
    @Results({
            @Result(column = "b_id",property = "id"),
            @Result(column = "book_image", property = "bookImage"),
            @Result(column = "publish_date",property = "publishDate"),
            @Result(column = "created_at",property = "cratedAt"),
            @Result(column = "b_status",property = "status"),

           //@Result(column = "c_id",property = "category.id"),
           // @Result(column = "name",property = "category.name"),
            //@Result(column = "created_at",property = "cratedAt"),
            @Result(column = "category",property = "category",one = @One(select = "getCategoryById"))

    })
    List<Book> getAllBooks();

    @Select("select * from tb_category where id = #{category_id}")
    @Results({

            @Result(column = "created_at",property ="createdAt" )
    })
    Category getCategoryById(@Param("category_id") Integer id);


    @Select("SELECT b.id, b.title,b.book_image as bookImage," +
            "b.isbn as ISBN,c.name as categoryName" +
            " from tb_book b inner join tb_category c on b.category_id=c.id")
    List<BookResponse> getAllBookResponse();




}