package com.example.demo.controllers.categories;


import com.example.demo.model.Category;
import com.example.demo.servies.CategoryServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/admin/categories")
public class CategoryControllers {

    private CategoryServices categoryServices;

    public CategoryControllers(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }

    @GetMapping("/all")
    public String getAllCategories( @RequestParam(required = false) String name, ModelMap modelMap){

        List<Category> categories =this.categoryServices.getAllCategory(name);
        modelMap.addAttribute("categories", categories);

        return "admin/categories/categories-all";


    }



}





