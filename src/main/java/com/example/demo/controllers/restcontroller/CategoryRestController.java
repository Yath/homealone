package com.example.demo.controllers.restcontroller;


import com.example.demo.model.Category;
import com.example.demo.servies.CategoryServices;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/categories")
public class CategoryRestController {

    private CategoryServices categoryServices;

    public CategoryRestController(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }

    @GetMapping("")
    public ResponseEntity<List<Category>> getAllCategory(String name){

        List<Category> categories =this.categoryServices.getAllCategory(name);

        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Map<String,Object>>save(@RequestBody Category category){

        Map<String,Object> response = new HashMap<>();

        boolean status = this.categoryServices.save(category);

        if (status == true){
            response.put("message","save category successfully!");
            response.put("category",category);
            response.put("status", true);

            return ResponseEntity.ok(response);
        }else {

            response.put("message","save category failed");
            response.put("category",category);
            response.put("status",false);

            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/batch")
    public ResponseEntity<Map<String,Object>>saveBatch(@RequestBody List<Category> category){

        Map<String,Object> response = new HashMap<>();

        boolean status = this.categoryServices.saveBatch(category);

        if (status == true){
            response.put("message","saveBatch category successfully!");
           response.put("category",category);
            response.put("status", true);

            return ResponseEntity.ok(response);
        }else {

            response.put("message","saveBatch category failed");
            response.put("category",category);
            response.put("status",false);

            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }






}
