package com.example.demo.controllers.restcontroller;

import com.example.demo.model.User;
import com.example.demo.servies.UserServices;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController {

    private UserServices userServices;

    public UserRestController(UserServices userServices) {

        this.userServices = userServices;
    }

    @GetMapping("")
    public List<User> getAllUsers(){

        return this.userServices.getAllUsers();
    }

    @PostMapping("")
    public String saveUser(@RequestBody User user){

        int status = this.userServices.saveUser(user);

        if (status>0){
            return "Save user successfully!!!";
        }else
            return "Save user failed";
    }

    @PutMapping("")
    public ResponseEntity<String> updateUser(@RequestBody User user){

        int status=this.userServices.updateUser(user);

        if (status>0){
            //return new ResponseEntity<>("Update user successfully", HttpStatus.OK);
            return ResponseEntity.ok("Update user successfully!");
        }
        else
            return new ResponseEntity<>("Update user failed!", HttpStatus.NOT_FOUND);
    }

    //@RequestMapping(value="{id}",method = RequestMethod.DELETE)
    @DeleteMapping("/{id}")
    public ResponseEntity<String> updateUser(@PathVariable Integer id){

        int status=this.userServices.deleteUser(id);

        if (status>0){
            //return new ResponseEntity<>("Update user successfully", HttpStatus.OK);
            return ResponseEntity.ok("Delete user successfully!");
        }
        else
            return new ResponseEntity<>("Delete user failed!", HttpStatus.NOT_FOUND);
    }


}

