package com.example.demo.controllers.restcontroller;


import com.example.demo.model.Book;
import com.example.demo.model.Category;
import com.example.demo.model.responses.BookResponse;
import com.example.demo.repositories.BookRepository;
import com.example.demo.servies.BookServices;
import com.example.demo.servies.CategoryServices;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/books")
public class BookRestController {


    private BookServices bookServices;

    public BookRestController(BookServices bookServices) {
        this.bookServices = bookServices;
    }

    //todo==        Can do like this...........
//
//    @GetMapping("")
//    public ResponseEntity<List<Book>> getAllBooks(){
//
//        List<Book> books =this.bookServices.getAllBooks();
//
//        return new ResponseEntity<>(books, HttpStatus.OK);
//    }

    //todo==         Or this Method...........

    @GetMapping("")
    public ResponseEntity<Map<String, Object>> getAllBook() {

        Map<String, Object> response = new HashMap<>();

        List<Book> books = this.bookServices.getAllBooks();

        if (books.size() > 0) {
            response.put("status", true);
            response.put("message", "Get book successfully!");
            response.put("data", books);

            return new ResponseEntity<>(response,HttpStatus.OK);
        }
        else
        {
            response.put("status", false);
            response.put("message", "Get book failed!");
            response.put("data", books);

            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/response")
    public ResponseEntity<Map<String, Object>> getAllBooksResponse() {

        Map<String, Object> response = new HashMap<>();

        List<BookResponse> bookResponses = this.bookServices.getAllBookResponse();

        if (bookResponses.size() > 0) {
            response.put("status", true);
            response.put("message", "Get book Response successfully!");
            response.put("data", bookResponses);

            return new ResponseEntity<>(response,HttpStatus.OK);
        }
        else
        {
            response.put("status", false);
            response.put("message", "Get book Response failed!");
            response.put("data", bookResponses);

            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }

    }
}
