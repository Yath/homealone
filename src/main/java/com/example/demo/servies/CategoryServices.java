package com.example.demo.servies;

import com.example.demo.model.Category;

import java.util.List;

public interface CategoryServices {


    List<Category> getAllCategory(String name);

    boolean save(Category category);

    boolean saveBatch(List<Category> categories);

}
