package com.example.demo.servies;

import com.example.demo.model.User;

import java.util.List;

public interface UserServices {

    List<User> getAllUsers();

    int saveUser(User user);

    int updateUser(User user);

    int deleteUser(Integer id);

}
