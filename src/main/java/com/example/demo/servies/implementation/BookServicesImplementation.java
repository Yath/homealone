package com.example.demo.servies.implementation;

import com.example.demo.model.Book;
import com.example.demo.model.Category;
import com.example.demo.model.responses.BookResponse;
import com.example.demo.repositories.BookRepository;
import com.example.demo.servies.BookServices;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServicesImplementation implements BookServices {

    private BookRepository bookRepository;

    public BookServicesImplementation(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBooks() {
        return this.bookRepository.getAllBooks();
    }


    @Override
    public List<BookResponse> getAllBookResponse() {
        return this.bookRepository.getAllBookResponse();
    }

}
