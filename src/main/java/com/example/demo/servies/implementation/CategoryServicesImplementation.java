package com.example.demo.servies.implementation;

import com.example.demo.model.Category;
import com.example.demo.repositories.CategoryRepository;
import com.example.demo.servies.CategoryServices;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryServicesImplementation  implements CategoryServices {

    private CategoryRepository categoryRepository;

    public CategoryServicesImplementation(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory(String name) {
        return this.categoryRepository.getAllCategory(name);
    }


    @Override
    public boolean save(Category category) {
        return this.categoryRepository.save(category);
    }

    @Override
    public boolean saveBatch(List<Category> categories) {
        return this.categoryRepository.saveBatch(categories);
    }
}
