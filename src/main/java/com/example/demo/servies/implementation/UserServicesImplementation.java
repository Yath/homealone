package com.example.demo.servies.implementation;

import com.example.demo.model.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.servies.UserServices;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServicesImplementation implements UserServices {


    private UserRepository userRepository;

    public UserServicesImplementation(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return this.userRepository.getAllUsers();
    }

    @Override
    public int saveUser(User user) {
        return this.userRepository.saveUser(user);
    }

    @Override
    public int updateUser(User user) {
        return this.userRepository.updateUser(user);
    }

    @Override
    public int deleteUser(Integer id) {
        return this.userRepository.deleteUser(id);
    }
}
