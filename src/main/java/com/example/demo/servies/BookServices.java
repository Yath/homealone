package com.example.demo.servies;

import com.example.demo.model.Book;
import com.example.demo.model.responses.BookResponse;


import java.util.List;


public interface BookServices {

    List<Book> getAllBooks();


    List<BookResponse> getAllBookResponse();




}
